﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FormulaImage : MonoBehaviour
{

    public Sprite[] BeginnerSprites;
    public Sprite[] IntermediateSprites;
    public Sprite[] HardSprites;

    public enum FormulaType
    {
        Beginner,
        Intermediate,
        Hard
    }

    public Image ImageElement;
	// Use this for initialization
	void Start () {
		
	}

    public void SetFormulaImage(FormulaType type, int index)
    {
        switch (type)
        {
            case FormulaType.Beginner:
                ImageElement.sprite = BeginnerSprites[index];
                break;
            case FormulaType.Intermediate:
                ImageElement.sprite = IntermediateSprites[index];
                break;
            case FormulaType.Hard:
                ImageElement.sprite = HardSprites[index];
                break;
            default:
                throw new ArgumentOutOfRangeException("type", type, null);
        }
         
    }
	// Update is called once per frame
	void Update () {
		
	}
}
