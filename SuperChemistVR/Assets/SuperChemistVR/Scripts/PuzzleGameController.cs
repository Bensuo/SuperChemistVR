﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PuzzleGameController : MonoBehaviour
{
    public Transform Spotlight;

    public Text TimeText;

    public Text FormulaText;

    public Text StartText;

    public Text FinishText;

    public Text FailText;

    public FormulaImage FormulaImage;

    public Text[] DifficultyText;

    public List<string> BeginnerFormulas;
    public List<string> IntermediateFormulas;
    public List<string> HardFormulas;

    public PuzzleManager PuzzleManager;

    public Transform PlayerController;

    public float MaxPuzzleTime = 300.0f;

    public int MaxTries;

    private int falseMoves;

    private float puzzleStartTime;

    private float currentPuzzleTime;

    private int totalScore;

    private bool puzzleComplete;
    private int selectedDifficulty;
    private float deadzone = 0.5f;
    private List<string> ActiveFormulas;
    private bool canSwitch = false;
    enum GameState
    {
        Start,
        SelectDifficulty,
        Playing,
        Finished,
        Failed
    }

    private GameState currentState;

	// Use this for initialization
	void Start ()
	{
	    currentState = GameState.Start;
	}
	
	// Update is called once per frame
	void Update ()
	{
        switch (currentState)
        {
            case GameState.Start:
                if (OVRInput.GetDown(OVRInput.RawButton.A) || OVRInput.GetDown(OVRInput.RawButton.X))
                {
                    currentState = GameState.SelectDifficulty;
                    StartText.enabled = false;
                    foreach (var text in DifficultyText)
                    {
                        text.enabled = true;
                    }
                }
                break;
            case GameState.SelectDifficulty:
                for (int i = 0; i < DifficultyText.Length; i++)
                {
                    if (i == selectedDifficulty)
                    {
                        DifficultyText[i].color = Color.white;
                    }

                    else
                    {
                        DifficultyText[i].color = Color.black;
                    }
                }
                if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).y < -deadzone)
                {
                    if (canSwitch)
                    {
                        selectedDifficulty++;
                        canSwitch = false;
                    }
                }
                else if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).y > deadzone)
                {
                    if (canSwitch)
                    {
                        selectedDifficulty--;
                        canSwitch = false;
                    }
                }
                else
                {
                    canSwitch = true;
                }
                selectedDifficulty = selectedDifficulty > 2 ? 2 : selectedDifficulty < 0 ? 0 : selectedDifficulty;
                if (OVRInput.GetDown(OVRInput.RawButton.A) || OVRInput.GetDown(OVRInput.RawButton.X))
                {
                    currentState = GameState.Playing;
                    switch (selectedDifficulty)
                    {
                        case 0:
                            ActiveFormulas = BeginnerFormulas;
                            break;
                        case 1:
                            ActiveFormulas = IntermediateFormulas;
                            break;
                        case 2:
                            ActiveFormulas = HardFormulas;
                            break;
                    }
                    TimeText.enabled = true;
                    FormulaText.enabled = true;
                    FormulaImage.ImageElement.enabled = true;
                    Spotlight.gameObject.SetActive(true);
                    foreach (var text in DifficultyText)
                    {
                        text.enabled = false;
                    }
                    PuzzleManager.enabled = true;
                    NextPuzzle();
                    puzzleStartTime = Time.time;
                    
                }

                
                break;
            case GameState.Playing:
                if (!puzzleComplete)
                {
                    currentPuzzleTime = MaxPuzzleTime - (Time.time - puzzleStartTime);
                }

                TimeSpan timespan = TimeSpan.FromSeconds(currentPuzzleTime);

                TimeText.text = "Time: " + timespan.Minutes.ToString("00") + ":" + timespan.Seconds.ToString("00") + "s";
                if (currentPuzzleTime <= 0)
                {
                    currentState = GameState.Failed;
                    TimeText.enabled = false;
                    FormulaText.enabled = false;
                    FormulaImage.ImageElement.enabled = false;
                    FailText.enabled = true;

                }

                break;
            case GameState.Finished:
            case GameState.Failed:
                if (OVRInput.GetDown(OVRInput.RawButton.A) || OVRInput.GetDown(OVRInput.RawButton.X))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }

                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }

    }

    public void OnFailedPlacement()
    {
        falseMoves--;

    }

    public void OnPuzzleComplete()
    {
        puzzleComplete = true;
        currentPuzzleTime = MaxPuzzleTime - (Time.time - puzzleStartTime);
    }
    public void NextPuzzle()
    {


        if (ActiveFormulas.Count > 0)
        {
            string prefix = "";
            switch (selectedDifficulty)
            {
                case 0:
                    prefix = "Beginner/";
                    break;
                case 1:
                    prefix = "Intermediate/";
                    break;
                case 2:
                    prefix = "Hard/";
                    break;
            }

            PuzzleManager.LoadNewFormula(prefix + ActiveFormulas[0]);
            ActiveFormulas.RemoveAt(0);
            foreach (var grabber in PlayerController.GetComponentsInChildren<OVRGrabber>())
            {
                grabber.OnReset();
            }

            //FormulaText.text = "Formula:\n" + PuzzleManager.GetTargetFormula().TextRepresentation;
            puzzleComplete = false;
            FormulaImage.SetFormulaImage((FormulaImage.FormulaType) selectedDifficulty, 4-ActiveFormulas.Count);
        }
        else
        {
            currentState = GameState.Finished;
            TimeText.enabled = false;
            FormulaText.enabled = false;
            FormulaImage.ImageElement.enabled = false;
            FinishText.enabled = true;
            
            totalScore = (int)currentPuzzleTime * 100 - falseMoves * 200;
            FinishText.text = "Finished!\nScore: " + totalScore + "\nMistakes: " + falseMoves;
            totalScore = totalScore < 0 ? 0 : totalScore;
            PuzzleManager.enabled = false;
        }
        

    }
}
