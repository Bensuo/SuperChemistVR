﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;


public class AtomSpawnBox : MonoBehaviour
{
    public Transform AtomPrefab;

    public List<FormulaNode.AtomType> TypesToSpawn;

    public int MaxActivePerType;

    public Collider SpawnArea;

    private Dictionary<FormulaNode.AtomType, List<Atom>> ActiveAtoms = new Dictionary<FormulaNode.AtomType, List<Atom>>();

    private float nextSpawnTimestamp;

    private const float spawnCooldown = 0.5f;
	// Use this for initialization
	void Start ()
	{

	    nextSpawnTimestamp = Time.time + spawnCooldown;
	}
	
	// Update is called once per frame
	void Update () {
	    foreach (var type in ActiveAtoms)
	    {
	        if (nextSpawnTimestamp <= Time.time && type.Value.Count < MaxActivePerType)
	        {
	            Atom atom = Instantiate(AtomPrefab, GetRandomPositionInSpawnArea(), Quaternion.identity).GetComponentInChildren<Atom>();
	            atom.AtomType = type.Key;
	            atom.GetComponentInParent<Renderer>().material.color = AtomTypeToColour(type.Key);
	            atom.AtomColor = AtomTypeToColour(type.Key);

                ActiveAtoms[type.Key].Add(atom);
	            nextSpawnTimestamp = Time.time + spawnCooldown;
            }
	    }
	}

    public void Clear()
    {
        foreach (var type in ActiveAtoms)
        {
            foreach (var atom in type.Value)
            {
                DestroyAtom(atom);
            }
        }
        ActiveAtoms.Clear();
    }

    public void SetTypesToSpawn(List<FormulaNode.AtomType> typesToSpawn)
    {
        TypesToSpawn = typesToSpawn;
        foreach (var type in TypesToSpawn)
        {
            ActiveAtoms[type] = new List<Atom>();
        }
    }
    public void OnAddedToPuzzle(Atom atom)
    {
        ActiveAtoms[atom.AtomType].Remove(atom);
    }

    public void OnDespawn(Atom atom)
    {
        ActiveAtoms[atom.AtomType].Remove(atom);
        DestroyAtom(atom);
    }
    public void DestroyAtom(Atom atom)
    {
        Destroy(atom.transform.parent.parent.gameObject);
    }

    Vector3 GetRandomPositionInSpawnArea()
    {
        float x = UnityEngine.Random.Range(-0.5f, 0.5f) * SpawnArea.bounds.size.x;
        float y = UnityEngine.Random.Range(-0.5f, 0.5f) * SpawnArea.bounds.size.y;
        float z = UnityEngine.Random.Range(-0.5f, 0.5f) * SpawnArea.bounds.size.z;
        return SpawnArea.transform.position + new Vector3(x,y,z);
    }

    public static Color AtomTypeToColour(FormulaNode.AtomType atomType)
    {
        switch (atomType)
        {
            case FormulaNode.AtomType.Hydrogen:
                return Color.white;
                break;
            case FormulaNode.AtomType.Oxygen:
                return Color.red;
                break;
            case FormulaNode.AtomType.Carbon:
                return Color.black;
                break;
            case FormulaNode.AtomType.Nitrogen:
                return Color.blue;
                break;
            case FormulaNode.AtomType.Chlorine:
                return new Color(0, 0.6f, 0);
                break;
            case FormulaNode.AtomType.Sulfur:
                return Color.yellow;
                break;
            case FormulaNode.AtomType.Fluorine:
                return Color.green;
                break;
            default:
                throw new ArgumentOutOfRangeException("atomType", atomType, null);
        }
    }
}
