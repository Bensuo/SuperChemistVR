﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BondPoint : MonoBehaviour
{

    public FormulaNode.BondType BondType;

    public FormulaNode.FormulaConnection Connection;

    public Vector3 Angle;

    public Color Color;

    public void OnSelect()
    {
        foreach (var r in GetComponentsInChildren<Renderer>())
        {
            r.material.color = Color.magenta;
        }
    }
    public void OnClear()
    {
        foreach (var r in GetComponentsInChildren<Renderer>())
        {
            r.material.color = Color;
        }
    }
    // Use this for initialization
    void Start () {

        foreach (var r in GetComponentsInChildren<Renderer>())
        {
            r.material.color = Color;
        }
    }
	
	// Update is called once per frame
	void Update () {
	    if (Connection != null && Connection.IsActive)
	    {
	        foreach (var r in GetComponentsInChildren<Renderer>())
	        {
	            r.material.color = Color;
	        }

	    }
    }
}
