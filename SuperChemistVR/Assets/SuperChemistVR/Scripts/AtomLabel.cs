﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomLabel : MonoBehaviour
{
    public Atom Atom;

    private Transform _cameraTransform;

    private Canvas _canvas;
	// Use this for initialization
	void Start ()
	{
	    _cameraTransform = GameObject.Find("CenterEyeAnchor").transform;
	    _canvas = GetComponentInChildren<Canvas>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if ((Atom.GetComponentInParent<OVRGrabbable>() != null && (Atom.GetComponentInParent<OVRGrabbable>().isGrabbed)) || (Atom.FormulaNode != null && Atom.FormulaNode.IsActive))
	    {
	        float dist = Vector3.Distance(transform.parent.position, _cameraTransform.position);
	        float angle = Vector3.Dot(_cameraTransform.forward, transform.parent.position - _cameraTransform.position);

	        if (dist < 0.2f)
	        {
	           _canvas.gameObject.SetActive(false);

	        }

	        if (dist > 0.35f)
	        {
	            _canvas.gameObject.SetActive(true);
                //transform.Rotate(0, -180, 0);

            }
	    }
	    else
	    {
	        _canvas.gameObject.SetActive(false);
        }

	    transform.position = transform.parent.position + transform.right * -0.3f;
	    transform.rotation = Quaternion.Slerp(transform.rotation,
	        Quaternion.LookRotation(transform.parent.position - _cameraTransform.position), 0.1f);
    }
}
