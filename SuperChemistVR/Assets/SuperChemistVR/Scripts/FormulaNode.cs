﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;


public class FormulaNode
{
    public enum BondType
    {
        Single,
        Double,
        Triple
    };

    public enum AtomType
    {
        Hydrogen,
        Oxygen,
        Carbon,
        Nitrogen,
        Chlorine,
        Sulfur,
        Fluorine
    };

    public class FormulaConnection
    {
        public FormulaNode NodeA;
        public FormulaNode NodeB;
        public BondType BondType;
        public bool IsActive;
        public Vector3 Angle;
        public FormulaConnection(FormulaNode nodeA, FormulaNode nodeB, BondType type, Vector3 angle)
        {
            this.NodeA = nodeA;
            this.NodeB = nodeB;
            this.BondType = type;
            this.IsActive = false;
            this.Angle = angle;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            var other = obj as FormulaConnection;
            bool nodesEqual = (NodeA.Type == other.NodeA.Type && NodeB.Type == other.NodeB.Type) ||
                              (NodeA.Type == other.NodeB.Type && NodeB.Type == other.NodeA.Type);
            return nodesEqual && BondType == other.BondType;
        }
    };

    [System.Serializable]
    public class NodeSerializable
    {
        public int ID;
        public string AtomType;
        public ConnectionSerializable[] Connections;
    }
    [System.Serializable]
    public class ConnectionSerializable
    {
        public int A;
        public int B;
        public string BondType;
        public float[] Angle;
    }

    public bool IsActive;
    public AtomType Type;
    public List<FormulaConnection> Connections;

    public FormulaNode(AtomType type)
    {
        this.IsActive = false;
        this.Connections = new List<FormulaConnection>();
        this.Type = type;
    }

    public void AddConnection(FormulaConnection connection)
    {
        Connections.Add(connection);
    }


}



