﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;



public class Formula
{
    [System.Serializable]
    private class FormulaSerializable
    {
        public string Name;
        public string TextRepresentation;
        public FormulaNode.NodeSerializable[] Nodes;
        public FormulaNode.ConnectionSerializable[] Connections;
    }
    public List<FormulaNode> Nodes;
    public FormulaNode RootNode;
    public List<FormulaNode.AtomType> TypesUsed;
    public string Name;
    public string TextRepresentation;
    public Formula()
    {
        this.Nodes = new List<FormulaNode>();
        this.TypesUsed = new List<FormulaNode.AtomType>();
    }
    public Formula(FormulaNode rootNode)
    {
        this.Nodes = new List<FormulaNode>();
        Nodes.Add(rootNode);
    }
    

    public static Formula CreateFromJson(string path)
    {
        //string jsonString = File.ReadAllText(path);
        string jsonString = Resources.Load<TextAsset>(path).text;
        FormulaSerializable ser = JsonUtility.FromJson<FormulaSerializable>(jsonString);
        Formula formula = new Formula();
        //Create nodes
        foreach (var node in ser.Nodes)
        {
            FormulaNode.AtomType atomType = (FormulaNode.AtomType) Enum.Parse(typeof(FormulaNode.AtomType), node.AtomType);
            if (!formula.TypesUsed.Contains(atomType))
            {
                formula.TypesUsed.Add(atomType);
            }
            formula.Nodes.Add(new FormulaNode(atomType));
        }

        foreach(var connection in ser.Connections)
        {
            FormulaNode nodeA = formula.Nodes[connection.A];
            FormulaNode nodeB = formula.Nodes[connection.B];
            FormulaNode.BondType bondType =
                (FormulaNode.BondType)Enum.Parse(typeof(FormulaNode.BondType), connection.BondType);
            Vector3 angle = new Vector3(connection.Angle[0], connection.Angle[1], connection.Angle[2]);
            MakeConnection(nodeA, nodeB, bondType, angle);
        }

        formula.RootNode = formula.Nodes[0];
        formula.Name = ser.Name;
        formula.TextRepresentation = ser.TextRepresentation;
        return formula;
    }

    public static void MakeConnection(FormulaNode nodeA, FormulaNode nodeB, FormulaNode.BondType type, Vector3 angle)
    {
        var connection = new FormulaNode.FormulaConnection(nodeA, nodeB, type, angle);
        nodeA.AddConnection(connection);
        nodeB.AddConnection(connection);
    }
}
