﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public Transform AtomPrefab;
    public AtomSpawnBox AtomSpawnBox;
    public AudioClip WinSound;
    public AudioClip CorrectSound;
    public AudioClip FalseSound;
    public PuzzleGameController GameController;
    private Atom rootAtom;
    private Formula TargetFormula;
    private AudioSource _audioSource;
    private bool PuzzleComplete = false;
	// Use this for initialization
	void Start ()
	{
	    _audioSource = GetComponent<AudioSource>();
	    AtomSpawnBox.enabled = true;
	}

    void LoadFromFile(string fname)
    {
        String app_path = Application.dataPath;
        TargetFormula = Formula.CreateFromJson("JSON/Formulas/" + fname);
        var root = TargetFormula.RootNode;
        AtomSpawnBox.SetTypesToSpawn(TargetFormula.TypesUsed);

        var obj = Instantiate(AtomPrefab, transform.position, transform.rotation);
        rootAtom = obj.GetComponentInChildren<Atom>();
        rootAtom.FormulaNode = root;
        rootAtom.FormulaNode.IsActive = true;
        rootAtom.AtomType = root.Type;
        rootAtom.AtomColor = AtomSpawnBox.AtomTypeToColour(root.Type);
        rootAtom.MakeActive();
    }
	// Update is called once per frame
	void Update ()
	{
	    if (!PuzzleComplete)
	    {
	        PuzzleComplete = CheckPuzzleComplete();
	        if (PuzzleComplete)
	        {
	            _audioSource.PlayOneShot(WinSound);
	            GameController.OnPuzzleComplete();

	        }
        }

	    if (PuzzleComplete && _audioSource.isPlaying == false)
	    {
	        AtomSpawnBox.enabled = false;
            GameController.NextPuzzle();


	    }
    }

    void DestroyPuzzle()
    {
        if (rootAtom != null)
        {
            Destroy(rootAtom.transform.parent.parent.gameObject);
            rootAtom = null;
        }
    }

    public Formula GetTargetFormula()
    {
        return TargetFormula;
    }
    public void LoadNewFormula(string fname)
    {
        DestroyPuzzle();
        AtomSpawnBox.Clear();
        LoadFromFile(fname);
        PuzzleComplete = false;
        AtomSpawnBox.enabled = true;
    }
    public bool AttemptConnection(Atom existingAtom, Atom newAtom, FormulaNode.FormulaConnection connection)
    {
        if (existingAtom.FormulaNode != null && newAtom.FormulaNode == null)
        {
            FormulaNode newNode = existingAtom.FormulaNode == connection.NodeA ? connection.NodeB : connection.NodeA;
            
            if (newNode.Type == newAtom.AtomType)
            {
                newAtom.FormulaNode = newNode;
            }
            //newAtom.FormulaNode = existingAtom.FormulaNode.HasFreeConnection(newAtom.AtomType, connection);
            if (newAtom.FormulaNode != null)
            {
                connection.IsActive = true;
                AtomSpawnBox.OnAddedToPuzzle(newAtom);
                newAtom.FormulaNode.IsActive = true;
                _audioSource.PlayOneShot(CorrectSound);
            }
            else
            {
                if(!_audioSource.isPlaying) _audioSource.PlayOneShot(FalseSound);
                GameController.OnFailedPlacement();
            }
            
        }
        else
        {
            return false;
        }

        return newAtom.FormulaNode != null;
    }
    //void MakeConnection(FormulaNode nodeA, FormulaNode nodeB, FormulaNode.BondType bondType = FormulaNode.BondType.Single)
    //{
    //    TargetFormula.MakeConnection(nodeA, nodeB, bondType);
    //}
    bool CheckPuzzleComplete()
    {
        foreach (var node in TargetFormula.Nodes)
        {
            if (!node.IsActive)
            {
                return false;
            }
        }

        return true;
    }
}
