﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Remoting;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Atom : MonoBehaviour
{
    public FormulaNode.AtomType AtomType;

    public FormulaNode FormulaNode
    {
        get { return _formulaNode; }
        set { _formulaNode = value; }
    }

    public Transform SinglePrefab;
    public Transform DoublePrefab;
    public Transform TriplePrefab;
    public BondPoint[] BondPoints;
    public Color AtomColor;
    private BondPoint ActiveBondPoint;
    private FormulaNode _formulaNode = null;

    private Collider collider;

    private PuzzleManager puzzleManager;

    private bool canJoin = false;

    private OVRGrabbable _ovrGrabbable;
    // Use this for initialization
    void Start ()
    {
        collider = GetComponent<SphereCollider>();
        puzzleManager = GameObject.Find("PuzzleManager").GetComponent<PuzzleManager>();
        _ovrGrabbable = transform.parent.GetComponent<OVRGrabbable>();
        transform.parent.GetComponentInChildren<Text>().text = AtomType.ToString();
        GetComponentInParent<Renderer>().material.color = AtomColor;
        GetComponentInParent<Renderer>().material.SetFloat("Seed", Random.Range(-1, 1));
    }

    void OnTriggerEnter(Collider other)
    {
        if (_ovrGrabbable && _ovrGrabbable.isGrabbed)
        {
            if (FormulaNode == null && other.attachedRigidbody)
            {
                var otherAtom = other.GetComponent<Atom>();
                if (otherAtom != null)
                {
                    canJoin = true;
                }
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        
            if (FormulaNode == null && other.attachedRigidbody)
            {
                var otherAtom = other.GetComponent<Atom>();
                if (otherAtom != null)
                {
                    otherAtom.ClearBondPointSelection();
                    if (_ovrGrabbable && _ovrGrabbable.isGrabbed)
                    {
                        canJoin = false;
                    }
                }
            }
        
    }
    void OnTriggerStay(Collider other)
    {
        if (FormulaNode == null && other.attachedRigidbody)
        {
            var otherAtom = other.GetComponent<Atom>();
            if (otherAtom != null)
            {
                if (otherAtom.FormulaNode != null && otherAtom.FormulaNode.IsActive)
                {
                    otherAtom.SelectNearestBondPoint(transform.position);
                    if (!_ovrGrabbable.isGrabbed && canJoin)
                    {
                        BondPoint bondPoint = otherAtom.GetActiveBondPoint();
                        if (bondPoint == null)
                        {
                            return;
                        }
                        bool success = puzzleManager.AttemptConnection(otherAtom, this, bondPoint.Connection);
                        if (success)
                        {
                            
                            GameObject oldParent = transform.parent.parent.gameObject;
                            transform.parent.SetParent(otherAtom.transform.parent, false);
                            transform.parent.localPosition = new Vector3();
                            transform.parent.localRotation = Quaternion.identity;
                            Destroy(oldParent);
                            //transform.parent.rotation = Quaternion.identity;
                            Vector3 newPos = Vector3.Normalize(bondPoint.transform.position - otherAtom.transform.position) * 1.5f;
                            transform.parent.localPosition = Quaternion.Inverse(transform.rotation) * newPos;
                            MakeActive(bondPoint.Connection);
                        }
                        else
                        {
                            canJoin = false;
                        }
                    }
                }
            }
        }
    }
	// Update is called once per frame
	void Update () {
		
	}

    public void MakeActive(FormulaNode.FormulaConnection connection = null)
    {
        SetBondPoints(connection);
        OVRGrabbable grabbable = transform.parent.GetComponent<OVRGrabbable>();
        OVRGrabber grabber = grabbable.grabbedBy;
        if (grabber != null)
        {
            grabber.ForceRelease(grabbable);
        }
        
        Destroy(grabbable);
        _ovrGrabbable = null;
        transform.parent.GetComponent<Rigidbody>().isKinematic = true;
        transform.parent.GetComponent<Rigidbody>().velocity = new Vector3();
    }

    public void SelectNearestBondPoint(Vector3 pos)
    {
        if (FormulaNode != null)
        {
            float closestDist = float.MaxValue;
            BondPoint closest = null;
            foreach (var point in BondPoints)
            {
                if (point.GetComponent<BondPoint>().Connection.IsActive == false)
                {

                    float d = Vector3.Distance(pos, point.transform.position);
                    if (d < closestDist)
                    {
                        closestDist = d;
                        closest = point;
                    }
                }
            }
            ClearBondPointSelection();
            if (closest != null)
            {
                
                closest.OnSelect();
                ActiveBondPoint = closest;
            }
        }
    }

    public void ClearBondPointSelection()
    {
        if (ActiveBondPoint != null)
        {
            ActiveBondPoint.OnClear();
            ActiveBondPoint = null;
        }
    }

    public BondPoint GetActiveBondPoint()
    {
        return ActiveBondPoint;
    }

    float GetBondAngleForAxis(float otherAngle, bool flipAngle)
    {
        float newAngle = otherAngle;
        if (flipAngle)
        {
            if (newAngle != 0)
            {
                newAngle = newAngle > 0 ? -(180-newAngle) : 180 + newAngle;
            }
            else
            {
                newAngle = 180;
            }
        }

        return newAngle;
    }

    GameObject CreateBondPoint(FormulaNode.FormulaConnection connection)
    {
        //angle = angle >= 360 ? angle - 360 : angle; 
        Transform obj = null;
        switch (connection.BondType)
        {
            case FormulaNode.BondType.Single:
                obj = Instantiate(SinglePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                break;
            case FormulaNode.BondType.Double:
                obj = Instantiate(DoublePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                break;
            case FormulaNode.BondType.Triple:
                obj = Instantiate(DoublePrefab, new Vector3(0, 0, 0), Quaternion.identity);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        bool flipAngle = FormulaNode != connection.NodeA;
        float newX = GetBondAngleForAxis(connection.Angle.x, false);
        float newY = GetBondAngleForAxis(connection.Angle.y, false);
        float newZ = GetBondAngleForAxis(connection.Angle.z, flipAngle);
        Vector3 newAngle = new Vector3(0, newY, newZ);

        BondPoint bp = obj.GetComponentInChildren<BondPoint>();
        obj.SetParent(transform.parent, false);
        bp.transform.localPosition = transform.parent.up * 5.5f;
        
        obj.transform.Rotate(newAngle, Space.Self);
        bp.Connection = connection;
        bp.Angle = connection.Angle;
        bp.Color = AtomColor;
        return obj.gameObject;
    }
    void SetBondPoints(FormulaNode.FormulaConnection initialConnection = null)
    {
        
        //float angleStep = FormulaNode.Connections.Count == 1 ? 0.0f : 360.0f / (FormulaNode.Connections.Count +1);
        BondPoints = new BondPoint[FormulaNode.Connections.Count];
        int nextIndex = 0;
        if (initialConnection != null)
        {
            nextIndex++;
            BondPoints[0] = CreateBondPoint(initialConnection).GetComponentInChildren<BondPoint>();

        }

        int count = 0;
        while (count < FormulaNode.Connections.Count)
        {
            FormulaNode.FormulaConnection con = FormulaNode.Connections[count];
            if (con != initialConnection)
            {
                //float angle = angleStart + angleStep * (nextIndex);
                BondPoints[nextIndex] = CreateBondPoint(con).GetComponentInChildren<BondPoint>();
                nextIndex++;
            }

            count++;
        }

    }
}
