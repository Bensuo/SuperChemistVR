﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMagnet : MonoBehaviour
{
    public enum Hand
    {
        Left,
        Right
    }

    public Hand SetHand;
    public LineRenderer LineRenderer;

    private bool activateMagnets;

    private OVRInput.NearTouch nearTouch;
	// Use this for initialization
    void Start()
    {
        activateMagnets = false;
        if (SetHand == Hand.Left)
        {
            nearTouch = OVRInput.NearTouch.PrimaryIndexTrigger;
        }

        else
        {
            nearTouch = OVRInput.NearTouch.SecondaryIndexTrigger;
        }
    }

    void FixedUpdate()
    {
        if (activateMagnets)
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit info;
            Physics.Raycast(ray, out info, 10.0f);
            if (info.collider != null)
            {
                OVRGrabbable grab = info.collider.GetComponent<OVRGrabbable>();
                if (grab != null)
                {
                    if (Vector3.Distance(transform.position, info.collider.transform.position) > 0.5f)
                    {
                        info.collider.GetComponent<Rigidbody>().AddForce(Vector3.Normalize(transform.position - info.transform.position));
                    }
                }
            }
        }

        
    }
	// Update is called once per frame
	void Update ()
	{
	    activateMagnets = !OVRInput.Get(nearTouch);
	    LineRenderer.enabled = activateMagnets;
        LineRenderer.SetPosition(0, transform.position);
	    LineRenderer.SetPosition(1, transform.position + transform.forward * 10.0f);
    }
}
