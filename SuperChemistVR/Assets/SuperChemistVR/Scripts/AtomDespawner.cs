﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomDespawner : MonoBehaviour
{

    public AtomSpawnBox AtomSpawnBox;
	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerExit(Collider other)
    {
        Atom atom = other.GetComponentInChildren<Atom>();
        if (atom != null && atom.GetComponentInParent<OVRGrabbable>().isGrabbed == false)
        {
            AtomSpawnBox.OnDespawn(atom);
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
